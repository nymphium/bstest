(*
```ts:hoge.ts
export class Hoge {
  public hogev = "mm"
  public static hoge() : String {
    return "aa";
  }
}
```

```ts:huga.ts
export { Hoge } from './hoge';
```
*)

type hogeM = {
  hoge : unit -> string;
}
external hogeM : hogeM = "Hoge" [@@bs.module "./huga"]
external hoge : hogeM -> unit -> string = "hoge" [@@bs.send]

let hoge = hoge hogeM
let hoge' = hogeM.hoge

(* module M = struct *)
  (* let hoge = hoge *)
(* end *)

let _ = assert (hoge () = hoge' ())
(* let () = Js.log @@ hoge () *)
(* let () = Js.log @@ M.hoge () *)

