let m = Huga.Hoge.hogeT ~hoge: (fun () -> "hello")

let () = Js.log @@ m.hoge ()
let () = Js.log @@ Huga.Piyo.f ()
let () = Js.log @@ Huga.W.(x, y)
