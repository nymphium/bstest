type wM = {
  x : int;
  y : string;
} [@@bs.deriving abstract]

external w : wM = "W" [@@bs.module "./Huga"]

let x = xGet w
let y = yGet w
